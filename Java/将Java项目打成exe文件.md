# 将Java项目打成exe文件
### 写好项目打成jar包，使用exe4j将jar包转成exe文件

## idea 打jar包
	在File -> project Structure　选择Artifacts，点击＋，选择jar，选择From modules with Dependencies.
	
	或者

	点击右上角的Project Structure按钮。Project Structure对话框中选择Artifacts，如果之前没有创建过jar，需要点击加号新建一个

![](https://imgsa.baidu.com/exp/w=500/sign=f085920bc18065387beaa413a7dca115/cf1b9d16fdfaaf51904896fa875494eef11f7acf.jpg)
![](https://imgsa.baidu.com/exp/w=500/sign=cbd28f415282b2b7a79f39c401accb0a/95eef01f3a292df58a68bb50b7315c6035a873cf.jpg)

	选择From modules with Dependencies.添加

![](https://img.mukewang.com/5a54a266000159e906720331.png)
![](https://img.mukewang.com/5a54a35d0001040105140350.png)
	
	Module： 模块，选择需要打包的模块。如果程序没有分模块，那么只有一个可以选择的。
	MainClass：选择程序的入口类。
	extract to the target JAR：抽取到目标JAR。选择该项则会将所依赖的jar包全都打到一个jar文件中。
	copy to the output directory and link via manifest：将依赖的jar复制到输出目录并且使用manifest链接它们。
	Direct for META-INF/MANIFEST.MF： 如果上面选择了 "copy to ... "这一项，这里需要选择生成的manifest文件在哪个目录下。

	选择OK，会在刚才选择的文件夹下面生成一个META-INF文件夹，下面有一个MANIFEST.MF文件
	
	MANIFEST.MF
	主要一下几个：
	Manifest-Version： Manifest文件的版本，这个不用管。
	Class-Path： 描述lib包相对生成的jar的路径。
	Main-Class： 程序的入口类

选择菜单中的 build -> build artifacts... 


![](https://img.mukewang.com/5a54b8e70001b5cf03240212.png)

	此时页面中间会弹出要生成的jar包，选择刚刚构建的Artifacts，选择build或者rebuild

![](https://img.mukewang.com/5a54b970000152ce01990127.png)

	Build：只将主Jar包重新生成，不重新生成所依赖的Jar包。
	Rebuild： 将所有jar包重新生成。
	此时生成jar包已经完成


##  exe4j 使用教程

###文件下载
	https://www.ej-technologies.com/download/exe4j/files





1.  安装完成exe4j工具之后，双击会打开welcome界面，这个界面是用来输注册码用的，你要是不输，可以直接next。但若是不输入，双击生成的exe时，会alert出一段话，如图。必须点“确定”才可以执行程序。

	
	![](http://dl2.iteye.com/upload/attachment/0112/2636/026b03bb-d00f-3a63-97f1-c4581b51a78e.png)



2.  为了不让这个问题成为“客户反馈的问题”，咱们还是输入注册码吧。点击"Enter license"按钮，输入Name、Company、和License Key。前面两个可以随便输，key可以试试下面的。
 

		License Key:
	
		A-XVK258563F-1p4lv7mg7sav 
		A-XVK209982F-1y0i3h4ywx2h1 
		A-XVK267351F-dpurrhnyarva 
		A-XVK204432F-1kkoilo1jy2h3r 
		A-XVK246130F-1l7msieqiwqnq 
		A-XVK249554F-pllh351kcke50 
		A-XVK238729F-25yn13iea25i 
		A-XVK222711F-134h5ta8yxbm0


3.  Lisence输入完成了点击 “OK” 回到 welcome 界面，点击 “Next”，在“Choose project type”里面选在 “JAR in EXE mode" ，也就是用jar包来生成exe。 点击 Next。 
		![](http://dl2.iteye.com/upload/attachment/0112/2640/7a85d7c1-01d8-3568-b27f-a1dc5998b72a.jpg)
4.  目录配置

		也就是将哪个文件夹下面的东西打成exe。常规里面填个应用的简写名称，Direcotries里面选择需要生成exe的目录，也就是上面组织的目录结构。如图。最后Next。

	![](http://dl2.iteye.com/upload/attachment/0112/2642/d88dac88-1e6a-3836-8e22-b3f2efef4080.jpg)

5. exe执行程序配置。

		Executable name：就是exe执行文件的名称
		
		Icon File:exe执行文件的图标。这里选择image下面的ico文件。这里只能用ico文件作为图标。也可以取消前面的复选框不给当前exe设置图标，若是不设置系统会根据系统主题自动设置一个图标。
		
		Allow only a single ......：在一台电脑上只允许运行一个exe程序，若是允许多个，可以不勾选


		点击 Advanced Options 选择32-bit or 64-bit
	![](http://static.oschina.net/uploads/img/201402/17164022_StGW.png)

	![](http://static.oschina.net/uploads/img/201402/17164023_xTv4.png)

		最后点击”Next“。


	![](http://dl2.iteye.com/upload/attachment/0112/2644/0a855509-40f7-3c20-bfd6-b8c61081f79b.jpg)
6. invocation配置。

		也就是添加jar文件和指定主方法。点击绿色的”+“号，在弹出框里选择lib中的jar包（如图2），然后”OK“。这个只能一个一个的添加，要是用的jar包比较多加起来还挺麻烦的。 

	![](http://dl2.iteye.com/upload/attachment/0112/2645/bb7085fd-a55d-3b70-9d5b-000579cdf14c.jpg)

	![](http://dl2.iteye.com/upload/attachment/0112/2646/7917e31e-f884-35c8-ba78-6e772431ce39.jpg)

		lib中的jar都添加完了之后，指定main方法，点击”main class“后面的按钮，在弹出框里面找到程序的主方法，如图，添加进去。
	![](http://dl2.iteye.com/upload/attachment/0112/2656/0a1550a6-46cb-3ee9-bb29-f5237efff166.jpg)

7. 这两步都OK之后，最后的结果是：
![](http://dl2.iteye.com/upload/attachment/0112/2658/e750b0d2-6a4a-3892-ba01-87dbd5a8efac.jpg)

8. 有时候系统中除了用到jar包之外，还会用到dll文件，而上面的添加jar包的步骤中不能添加dll文件，将dll文件放在与exe同级目录下也一样会因为dll的问题导致系统出错，甚至于放到system32文件夹下也没用。我在弄这个系统的时候，就有dll文件，折腾了大半天，也找了好久的资料（找的过程中发现好多人在这一步也很苦恼），后来发现，其实exe4j早就考虑到这个问题了，就是上图中的Advanced Options，即：
	![](http://dl2.iteye.com/upload/attachment/0112/2998/b2e0f518-01d4-3f9d-8ba7-39a23d807d6d.png)

9. 可以通过这个添加任何非dll的文件所在的文件夹，操作很简单，点击之后弹出如下图的对话框，点击绿色“+”号， 选择dll文件所在目录，我这里是放在lib目录下的，所以选择lib，添加完之后，结果如下：
	![](http://dl2.iteye.com/upload/attachment/0112/2985/d3f48068-a4d8-3010-896b-ada9ac91158b.jpg)

10. 配置jre。

		配置jre的最小版本，即低于这个版本的Jre无法运行该程序。最大版本也是一样的。我用的jdk是1.6的，所以配置最小为1.6。最大的可以不设置。
	![](http://dl2.iteye.com/upload/attachment/0112/2660/8529984a-006b-3c9f-aba8-ebca3e94946e.jpg)

11. 接着点击“Advanced Options”-“Search sequence”，设置一下我们的JRE，因为客户的机器上未必有jre所以咱们自己带一个比较好。在弹出的框里面点击绿色的”+“号，在Define search sequence entry中选择Direcotry，并给定jre所在目录。然后”OK“。
	![](http://dl2.iteye.com/upload/attachment/0112/2662/13c64cc5-bf8f-3d2a-97a4-783a3672ebfe.jpg)

12. jre添加完成之后效果如下:
	![](http://dl2.iteye.com/upload/attachment/0112/2664/2c5a995c-1c17-3c57-944c-33da9e3db542.jpg)
	点击 Next
13. 之后出现Preferred VM 中选择Client hostpost VM 

	![](http://e.hiphotos.baidu.com/exp/w=500/sign=d519b7683dc79f3d8fe1e4308aa0cdbc/0eb30f2442a7d933d4b116e4ab4bd11373f0010f.jpg)

14. 完后的三步都是用默认操作，一路Next即可。直到如下界面：
	
	![](http://dl2.iteye.com/upload/attachment/0112/2666/7dfd15a4-f4a1-3a43-beb5-a15ac8ab1a7b.jpg)


15. 到这个界面说明已经生成完成了。 还需要说的有三点：

		1、Click here to Start the Application:这个按钮时用来测试执行程序的，点击该按钮即可启动exe执行程序。
		
		2、Save As：这个很重要。这个保存了以上运行步骤的过程，保存后会生成一个exe4j，以后如果需要将该程序重新打exe，直接运行这个exe4j文件即可，以后的各种配置都被存入这个文件了，基本上一路next就成，特别好用。
		
		3、Restart：这个restart有点误导人。Restart是重启的意思，在这的话更容易理解成重新生成。我开始弄的时候弄错了，想重新生成一下，于是点了这个，结果直接给我跳到第一步重新来了。所幸各种设置都还在，要是给我清空我不得重来一回。

16. 最后的结果应该这个样子的：

	![](http://dl2.iteye.com/upload/attachment/0112/2670/05468c16-b58d-35ab-8ab7-b2cf3576d746.jpg)

	都在同一目录下

17. 给客户的东西：

		新建一个文件夹，将JRE文件夹和生成的exe执行程序拷贝到文件夹下。
		若是系统里面用到了dll文件，这在一步还需要添加dll的文件信息，上文中有提到添加dll文件:是选择的dll所在的文件夹，这里也一样，将dll所在的文件夹复制过来与exe同级，再删掉里面的jar包。
		为什么这么处理呢？
		因为exe4j对于这些非jar文件不会像jar包一样包装进exe，它只记录它们的具体位置（也就是让我们选的dll目录），所以才需要我们手动添加dll文件及目录且目录名称一定要与选择的目录相同。
		最后，双击exe，测试程序效果是否有误。

