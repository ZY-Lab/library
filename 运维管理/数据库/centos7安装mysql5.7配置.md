## centos安装MySQL 5.7配置
1. 下载npm源
	 	wget http://dev.mysql.com/get/mysql57-community-release-el7-7.noarch.rpm
2. 安装rpm包，# rpm -ivh mysql57-community-release-el7-7.noarch.rpm
3. 用yum安装MySQL
		yum install mysql-community-server
4. 在log里获取随机的默认密码
		 grep "password" /var/log/mysqld.log
5. 在mysql下，如果修改密码时遇到 **ERROR 1819 (HY000): Your password does not satisfy the current policy requirements** 表示你的代码安全指数太低了，在etc/my.conf中修改，在[mysqld]之后添加
		#创建密码策略取消，方便设置密码
        validate_password=off
        #设置过期时间，以防密码失效
        default_password_lifetime=0
就可以随便建密码了
6. 进入mysql，更改密码
		 set password for root@localhost = password('123456')
7. 如果有需要改编码，在配置文件中，[mysqld]之前添加
		[client]
        #兼容表情字符编码
        default-character-set=utf8mb4

        [mysql] 
        default-character-set = utf8mb4
8. utf8m4编码格式还需要在[mysqld]下添加
		character-set-client-handshake = FALSE 
        character-set-server = utf8mb4 
        collation-server = utf8mb4_unicode_ci 
        init_connect=’SET NAMES utf8mb4’
